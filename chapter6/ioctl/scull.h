#ifndef _SCULL_H_
#define _SCULL_H_

#include <linux/ioctl.h> /* needed for the _IOW etc stuff used later */

#ifndef SCULL_MAJOR
#define SCULL_MAJOR 0   /* dynamic major by default */
#endif

#ifndef SCULL_NR_DEVS
#define SCULL_NR_DEVS 4    /* scull0 through scull3 */
#endif

#ifndef SCULL_P_NR_DEVS
#define SCULL_P_NR_DEVS 4  /* scullpipe0 through scullpipe3 */
#endif

#ifndef SCULL_QUANTUM
// 表示data指针数组中每一项能够占用多少字节
#define SCULL_QUANTUM 4000
#endif

#ifndef SCULL_QSET
// 表示scull_qset结构体中data指针数组有多少项
#define SCULL_QSET    1000
#endif

// 用该结构体来存放数据
struct scull_qset {
     // 这里的data会被看作是指针数组的首地址
	void **data;
	struct scull_qset *next;
};

// 通过该结构体来表示设备
struct scull_dev {
	struct scull_qset *data;  /* Pointer to first quantum set */
	int quantum;              // data数组每一项有多少字节的数据
	int qset;                 // 代表对应的每个scull_qset中的data数组有多少项
	unsigned long size;       /* amount of data stored here */
	unsigned int access_key;  /* used by sculluid and scullpriv */
	// 每个设备使用单独的信号量,这样便允许不同设备上的操作可以并行处理
	struct mutex lock;     /* mutual exclusion semaphore     */
	// 字符型设备的结构体
    struct cdev cdev;	  /* Char device structure		*/
};


/*
 * The different configurable parameters
 */
extern int scull_major;     /* main.c */
extern int scull_nr_devs;
extern int scull_quantum;
extern int scull_qset;

/*
 * Prototypes for shared functions
 */
int     scull_trim(struct scull_dev *dev);

ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
                   loff_t *f_pos);
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
                    loff_t *f_pos);
loff_t  scull_llseek(struct file *filp, loff_t off, int whence);
long     scull_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

// 第六章(阻塞I/O)新增内容
// int     scull_p_init(dev_t dev);
// void    scull_p_cleanup(void);


// IOCTL相关定义, 第6章ioctl部分的相关内容
// magic number 是与设备相关的数字，该驱动管理的设备的magic number设置为"K"
#define SCULL_IOC_MAGIC  'k'

#define SCULL_IOCRESET    _IO(SCULL_IOC_MAGIC, 0)

/*
 * S means "Set" through a ptr,
 * T means "Tell" directly with the argument value
 * G means "Get": reply by setting through a pointer
 * Q means "Query": response is on the return value
 * X means "eXchange": switch G and S atomically
 * H means "sHift": switch T and Q atomically
 */
// 下面这些语句用于定义命令对应的编号, _IO,_IOR,_IOW,_IOWR等都是用来构造命令编号的宏
// 第一个参数是与设备相关的数字，第二个参数则是设备内的序列号，而有些函数具有的第三个参数则是datatype
#define SCULL_IOCSQUANTUM _IOW(SCULL_IOC_MAGIC,  1, int)
#define SCULL_IOCSQSET    _IOW(SCULL_IOC_MAGIC,  2, int)
#define SCULL_IOCTQUANTUM _IO(SCULL_IOC_MAGIC,   3)
#define SCULL_IOCTQSET    _IO(SCULL_IOC_MAGIC,   4)
#define SCULL_IOCGQUANTUM _IOR(SCULL_IOC_MAGIC,  5, int)
#define SCULL_IOCGQSET    _IOR(SCULL_IOC_MAGIC,  6, int)
#define SCULL_IOCQQUANTUM _IO(SCULL_IOC_MAGIC,   7)
#define SCULL_IOCQQSET    _IO(SCULL_IOC_MAGIC,   8)
#define SCULL_IOCXQUANTUM _IOWR(SCULL_IOC_MAGIC, 9, int)
#define SCULL_IOCXQSET    _IOWR(SCULL_IOC_MAGIC,10, int)
#define SCULL_IOCHQUANTUM _IO(SCULL_IOC_MAGIC,  11)
#define SCULL_IOCHQSET    _IO(SCULL_IOC_MAGIC,  12)
#define SCULL_IOC_MAXNR 14 // 定义最大的序列号为14

#endif /* _SCULL_H_ */