/*
 * asynctest.c: use async notification to read stdin
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

int flag = 0;
void sighandler(int signo) {
    if(signo == SIGIO){
        flag = 1;  
        printf("SIGIO from driver, you can read now!\n");    
    }  
}

int main(int argc, char **argv) {
    int count, fd;
    int *buffer;
    struct sigaction action;

    buffer = (int*)malloc(count * sizeof(int));
    memset(&action, 0, sizeof(action));
    action.sa_handler = sighandler;
    action.sa_flags = 0;

    count = 100;  
    // 打开 /dev/scullp0 文件
    fd = open("/dev/scullp0", O_RDWR);
    // 当前进程接收到 SIGIO 信号后，便会调用 &action 指针指向的函数进行处理。
    sigaction(SIGIO, &action, NULL);
    /*
     * int fcntl(int fd, int cmd, long arg)。 F_SETOWN 命令用于设置 SIGIO 和 SIGURG 信号的进程ID和进程组ID
     * F_SETFL 命令则用于将文件状态标志设置为 arg 参数所指定的方式。
    */
    // 将当前进程的ID保存到文件描述符对应的 file 结构体的 f_owner 成员，即设置文件的所有权进程
    fcntl(fd, F_SETOWN, getpid());
    // 通过设置 F_SETFL 命令以及 FASYNC 标志启用异步通知。此时会调用驱动程序的 fasync 函数
    fcntl(fd, F_SETFL, fcntl(STDIN_FILENO, F_GETFL) | FASYNC);

    while(1) {
        sleep(2);
        // flag = true 则代表驱动程序已告知可以向设备文件写入数据
        if(!flag)
            continue;
        // 从设备文件读取数据
        read(fd, buffer, count * sizeof(int));
        printf("read data success!\n");
        break;
    }

    close(fd);
    return 0;
}
