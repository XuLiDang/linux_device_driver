#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h> 
#include "ioctl_test.h"

int main(int argc, char **argv)
{ 
	int fd;
	int ret;
	int result = 0;
	int *p = &result;

	fd = open("/dev/scull0", O_RDWR);
	if (fd < 0) { 
		perror("open");
		exit(-2);
	}

	// 测试 SCULL_IOCRESET 命令
	ret = ioctl(fd, SCULL_IOCRESET);	
	if (ret < 0) { 
		perror("ioctl SCULL_IOCRESET:");
		exit(-3);
	} 
	else 
		printf("SCULL_IOCRESET Success\n");

	// 测试 SCULL_IOCGQUANTUM 命令
	ret = ioctl(fd, SCULL_IOCGQUANTUM, p);
	if (ret < 0) { 
		perror("ioctl SCULL_IOCGQUANTUM:");
		exit(-4);
	}
	else 
		printf("SCULL_IOCGQUANTUM Success, scull_quantum = %d\n", *p);
	

	// 测试 SCULL_IOCQQUANTUM 命令
	ret = ioctl(fd, SCULL_IOCQQUANTUM);
	if (ret < 0) { 
		perror("ioctl SCULL_IOCQQUANTUM:");
		exit(-5);
	}
	else 
		printf("SCULL_IOCQQUANTUM Success, scull_quantum = %d\n", ret);

	return 0;
}