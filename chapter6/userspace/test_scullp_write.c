#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage()
{
    printf("usage: command \n");
    exit(0);
}

// 向设备写入并读取数据，最大字节数4000
int main(int argc,char **argv) {
    int i,fd,numbers;
    int *content;

    if(argc != 2)
        usage();
        
	// 通过控制台输入需要写入的数据个数
    numbers = atoi(argv[1]);  //将输入转化为int型
    // 打开 /dev/scullp0 文件
    fd = open("/dev/scullp0", O_RDWR);

    // 在content指针对应的缓存写数据0~999
    content = (int*)malloc(numbers * sizeof(int)); //写操作以Byte为单位
    for(i = 0; i < numbers; i++)
         *(content + i) = i;
	// 将数据写入scullp0
    write(fd, content, numbers * sizeof(int));
   
    close(fd);
    return 0;
}