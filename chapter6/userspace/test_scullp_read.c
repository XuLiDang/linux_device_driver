#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage()
{
    printf("usage: command \n");
    exit(0);
}

// 向设备写入并读取数据，最大字节数4000
int main(int argc,char **argv) {
    int i,fd,numbers;
    int *buffer;

    if(argc != 2)
        usage();
        
	// 通过控制台输入需要写入的数据个数
    numbers = atoi(argv[1]);  //将输入转化为int型
    // 打开 /dev/scullp0 文件
    fd = open("/dev/scullp0", O_RDWR);

    buffer = (int*)malloc(numbers * sizeof(int));
    // 将scullp0中的内容读出到buffer
    read(fd,buffer,numbers * sizeof(int)); 
     
    // 控制台打印
    for(i = 0; i < numbers; i++) {
        printf("%d\t", *(buffer + i));
        if((i + 1) % 10 == 0)
        printf("\n");
    }

    printf("\n");
    close(fd);
    return 0;
}