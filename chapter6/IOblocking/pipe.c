/*
 * pipe.c -- fifo driver for scull
 *
 * Copyright (C) 2001 Alessandro Rubini and Jonathan Corbet
 * Copyright (C) 2001 O'Reilly & Associates
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 */
 
#include <linux/module.h>
#include <linux/moduleparam.h>

#include <linux/kernel.h>	/* printk(), min() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/proc_fs.h>
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/fcntl.h>
#include <linux/poll.h>
#include <linux/cdev.h>
#include <asm/uaccess.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/seq_file.h>

// #include "proc_ops_version.h"

#include "scull.h"		/* local definitions */

struct scull_pipe {
        wait_queue_head_t inq, outq;       // 输入输出等待队列
        char *buffer, *end;                // 缓冲区的起始和结尾指针
        int buffersize;                    // 缓冲区大小
        char *rp, *wp;                     // 读取和写入的位置
        int nreaders, nwriters;            // 读者与写者的数量
        struct fasync_struct *async_queue; // 异步结构体
        struct mutex lock;                 // 互斥信号量
        struct cdev cdev;                  // 相关联的cdev结构
};

static int scull_p_nr_devs = SCULL_P_NR_DEVS;	/* number of pipe devices */
int scull_p_buffer =  SCULL_P_BUFFER;	/* buffer size */
dev_t scull_p_devno;			/* Our first device number */

module_param(scull_p_nr_devs, int, 0);	/* FIXME check perms */
module_param(scull_p_buffer, int, 0);

static struct scull_pipe *scull_p_devices; // scull_pipe结构体指针

static int scull_p_fasync(int fd, struct file *filp, int mode);
static int spacefree(struct scull_pipe *dev);

/*
 * Open and close
 */
static int scull_p_open(struct inode *inode, struct file *filp)
{
	struct scull_pipe *dev;
	// 根据 scull_pipe 结构体中的 cdev 成员的指针(inode->i_cdev)来获取
	// 指向 scull_pipe 结构体的指针，即识别正在打开的设备
	dev = container_of(inode->i_cdev, struct scull_pipe, cdev);
	// 填写file结构体中的 private_data 域为指向 scull_pipe 结构体的指针
	// file 结构体中定义了设备私有的数据结构
	filp->private_data = dev;

	// 申请互斥锁
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;
	// 若所打开设备的缓冲区指针为空
	if (!dev->buffer) {
		// 为设备的缓冲区申请内存空间
		dev->buffer = kmalloc(scull_p_buffer, GFP_KERNEL);
		// 若申请内存空间失败则释放互斥锁
		if (!dev->buffer) {
			mutex_unlock(&dev->lock);
			return -ENOMEM;
		}
	}

	// 初始化缓冲区的大小
	dev->buffersize = scull_p_buffer;
	// 初始化缓冲区的末尾指针
	dev->end = dev->buffer + dev->buffersize;
	// 初始化缓冲区的读取和写入的位置
	dev->rp = dev->wp = dev->buffer;

	// 从文件读取数据的进程+1
	if (filp->f_mode & FMODE_READ)
		dev->nreaders++;
	// 向文件写入数据的进程+1
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters++;
	// 释放互斥锁
	mutex_unlock(&dev->lock);

	// 调用nonseekable_open用于通知内核, 该设备不支持llseek
	return nonseekable_open(inode, filp);
}



static int scull_p_release(struct inode *inode, struct file *filp)
{
	struct scull_pipe *dev = filp->private_data;

	// 传入的第三个参数为mode, mode参数为0则代表从dev->async_queue中删除与传入的filp有关的
	// 所有fasync_struct结构体, 也意味着filp对应的文件禁用异步通知机制
	/* remove this filp from the asynchronously notified filp's */
	scull_p_fasync(-1, filp, 0);
	mutex_lock(&dev->lock);
	if (filp->f_mode & FMODE_READ)
		dev->nreaders--;
	if (filp->f_mode & FMODE_WRITE)
		dev->nwriters--;
	if (dev->nreaders + dev->nwriters == 0) {
		kfree(dev->buffer);
		dev->buffer = NULL; /* the other fields are not checked on open */
	}
	mutex_unlock(&dev->lock);
	return 0;
}


/*
 * Data management: read and write
 */
static ssize_t scull_p_read (struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scull_pipe *dev = filp->private_data;

	// 申请互斥锁
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;
	// 读取与写入位置相等,代表已无数据可读取
	while (dev->rp == dev->wp) { 
		// 释放互斥锁
		mutex_unlock(&dev->lock); 
		// 如果此次读取操作是非阻塞的，则立即返回
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;

		printk(KERN_ALERT "%s reading: going to sleep\n", current->comm);
		// 将当前进程放入读取等待队列中，后面会被其他进程调用 wake_up_interruptible 函数唤醒
		// 当该进程被其他进程唤醒时，会查看 dev->rp != dev->wp 是否为真，条件为真时该进程才会
		// 真正地被唤醒，否则会重新陷入睡眠状态。若函数返回非0, 则代表睡眠的进程是被某个信号唤醒的 
		// 而不是通过 wake_up_interruptible 函数，此时要返回 -ERESTARTSYS。
		if (wait_event_interruptible(dev->inq, (dev->rp != dev->wp)))
			return -ERESTARTSYS;

		// 若前面的函数返回的是0, 则代表睡眠的进程已被唤醒, 且所等待的条件已满足
		// 那么此时首先申请互斥锁
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
	}

	// 这里所用的缓冲区是一个循环的FIFO队列; 当wp > rp时, 能读取的数据量就为: wp - rp
	if (dev->wp > dev->rp)
		// 若count的值大于 dev->wp - dev->rp，那么count的值就变为 dev->wp - dev->rp
		count = min(count, (size_t)(dev->wp - dev->rp));

	// wp < rp，此时只能先读取rp至缓冲区尾部的这部分数据, 因此能读取的数据量就为: end - rp
	else 
		count = min(count, (size_t)(dev->end - dev->rp));
	if (copy_to_user(buf, dev->rp, count)) {
		mutex_unlock (&dev->lock);
		return -EFAULT;
	}

	// 更新读取位置
	dev->rp += count;
	// 若读取位置已达到缓冲区的尾部位置, 此时要将读取位置设为缓冲区的开始处
	if (dev->rp == dev->end)
		dev->rp = dev->buffer; /* wrapped */

	// 释放互斥锁
	mutex_unlock (&dev->lock);

	// 唤醒在等待队列当中的写者进程
	wake_up_interruptible(&dev->outq);
	printk(KERN_ALERT "%s did read %li bytes\n",current->comm, (long)count);
	return count;
}

/* Wait for space for writing; caller must hold device semaphore.  On
 * error the semaphore will be released before returning. */
static int scull_getwritespace(struct scull_pipe *dev, struct file *filp)
{	
	// 缓冲区满执行下面的循环
	while (spacefree(dev) == 0)
	{ 	
		// 建立并初始化一个等待队列项
		// wait_queue_t my_wait
		// init_wait(&my_wait);
		DEFINE_WAIT(wait);
		// 释放前面获得的互斥锁
		mutex_unlock(&dev->lock);
		// 如果用户请求是非阻塞I/O, 则立刻返回
		if (filp->f_flags & O_NONBLOCK)
			return -EAGAIN;

		printk(KERN_ALERT "%s writing: going to sleep\n",current->comm);
		// 将等待队列项添加到等待队列中, 并设置进程的状态
		prepare_to_wait(&dev->outq, &wait, TASK_INTERRUPTIBLE);
		// 再次检查等待的条件是否满足, 不满足则调用schedule函数切换其他进程运行
		// 若满足则跳过这里的if语句
		if (spacefree(dev) == 0)
			schedule();

		// 当不满足上面if语句或者睡眠进程被唤醒时, 就会调用finish_wait函数
		// 该函数主要会将当前进程的状态设置为running, 并将该进程从等待队列中移出
		finish_wait(&dev->outq, &wait);

		// 如果该进程是由信号唤醒的, 则此时要返回 -ERESTARTSYS
		if (signal_pending(current))
			return -ERESTARTSYS;
		// 如果该进程是由其他进程唤醒的, 则重新申请互斥锁
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
	}
	return 0;
}	

// 查询释放了多少缓冲区空间, 即缓冲区还有多少剩余的空间
// 如果该函数返回0则代表缓冲区是满的
static int spacefree(struct scull_pipe *dev)
{
	if (dev->rp == dev->wp)
		return dev->buffersize - 1;
	return ((dev->rp + dev->buffersize - dev->wp) % dev->buffersize) - 1;
}

static ssize_t scull_p_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{
	struct scull_pipe *dev = filp->private_data;
	int result;

	// 获得互斥锁
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	// 与read方法不同, write方法是将所有睡眠相关的代码放入了scull_getwritespace中
	result = scull_getwritespace(dev, filp);
	if (result)
		return result; /* scull_getwritespace called up(&dev->sem) */

	// 最终写入缓冲区的字节数为: 应用程序传入的字节数与缓冲区剩余空闲空间的最小值
	count = min(count, (size_t)spacefree(dev));
	if (dev->wp >= dev->rp)
		count = min(count, (size_t)(dev->end - dev->wp)); /* to end-of-buf */
	else /* the write pointer has wrapped, fill up to rp-1 */
		count = min(count, (size_t)(dev->rp - dev->wp - 1));
	printk(KERN_ALERT "Going to accept %li bytes to %p from %p\n", (long)count, dev->wp, buf);
	if (copy_from_user(dev->wp, buf, count)) {
		mutex_unlock(&dev->lock);
		return -EFAULT;
	}
	// 更新写入位置
	dev->wp += count;
	// 若写入位置已达到缓冲区的尾部位置, 此时要将写入位置设为缓冲区的开始处
	if (dev->wp == dev->end)
		dev->wp = dev->buffer; /* wrapped */
	mutex_unlock(&dev->lock);

	/* finally, awake any reader */
	wake_up_interruptible(&dev->inq);  /* blocked in read() and select() */

	// 查看设备结构体的async_queue队列是否为否, 不为空则代表需要向对应进程发送通知信号
	if (dev->async_queue)
		//当数据到达时调用kill_fasync()发送信号
		// void kill_fasync(struct fasync_struct **fa, int sig, int band);
		kill_fasync(&dev->async_queue, SIGIO, POLL_IN);
	printk(KERN_ALERT "%s did write %li bytes\n",current->comm, (long)count);
	return count;
}

// 应用程序可以通过调用 poll(struct pollfd fds[], nfds_t nfds, int timeout) 系统调用来查看一组打开的文件是否
// 可用（可读/可写）如果都不可用，那么当前进程就会被阻塞 timeout 秒或者有其他进程调用 wake_up() 函数唤醒当前进程
// 而如果有任一文件可用则会返回可用的文件数量
static unsigned int scull_p_poll(struct file *filp, poll_table *wait) {
	struct scull_pipe *dev = filp->private_data; //获得设备结构指针
	unsigned int mask = 0;

	/*
	 * The buffer is circular; it is considered full
	 * if "wp" is right behind "rp" and empty if the
	 * two are equal.
	 */
	// 申请获得互斥锁
	mutex_lock(&dev->lock);
	// 将scull_pipe结构体的两个等待队列添加到poll_table中，并且将当前进程分别加入到这两个等待队列中
	// 注意当前进程不会立刻休眠
	poll_wait(filp, &dev->inq,  wait);
	poll_wait(filp, &dev->outq, wait);
	// 根据数据的可读或可写状态设置相应的位掩码, 即告诉当前进程文件是否可以进行读写
	if (dev->rp != dev->wp)
		mask |= POLLIN | POLLRDNORM;	// 标记文件可读取
	if (spacefree(dev))
		mask |= POLLOUT | POLLWRNORM;	// 标记文件可写入
	// 释放互斥锁
	mutex_unlock(&dev->lock);
	return mask;
}



// 当一个打开的文件的FASYNC标志发生发生变化时，即filp->f_flags中的FASYNC标志发生了变化时
// 就会调用该文件对应file_operation结构体中的fasync函数，即filp->op->fasync函数
// 与该驱动程序相关的设备文件中的fasync函数被赋值为scull_p_fasync函数
static int scull_p_fasync(int fd, struct file *filp, int mode) {
	struct scull_pipe *dev = filp->private_data;
	// int fasync_helper(int fd, struct file *filp, int mode, struct fasync_struct **fa);
	// fasync_helper()的作用是创建一个fasync_struct结构体, 并将其放入到dev->async_queue异步通知队列中
	// 稍微具体点就是：首先为fasync_struct结构体分配内存空间随后将fasync_struct结构体与file结构体联系起来
	// (new->fa_file = filp, new是最新分配的fasync_struct结构体指针), 最后将该结构体放入到dev->async_queue队列中
	return fasync_helper(fd, filp, mode, &dev->async_queue);
}

// 打印设备相关信息,读取/proc/scullpipie文件时, 最终会调用这个函数
// static int scull_read_p_mem(struct seq_file *s, void *v)
// {
// 	int i;
// 	struct scull_pipe *p;

// 	#define LIMIT (PAGE_SIZE-200)        /* don't print any more after this size */
// 	seq_printf(s, "Default buffersize is %i\n", scull_p_buffer);
// 	for(i = 0; i<scull_p_nr_devs && s->count <= LIMIT; i++) {
// 		p = &scull_p_devices[i];
// 		if (mutex_lock_interruptible(&p->lock))
// 			return -ERESTARTSYS;
// 		seq_printf(s, "\nDevice %i: %p\n", i, p);
// /*		seq_printf(s, "   Queues: %p %p\n", p->inq, p->outq);*/
// 		seq_printf(s, "   Buffer: %p to %p (%i bytes)\n", p->buffer, p->end, p->buffersize);
// 		seq_printf(s, "   rp %p   wp %p\n", p->rp, p->wp);
// 		seq_printf(s, "   readers %i   writers %i\n", p->nreaders, p->nwriters);
// 		mutex_unlock(&p->lock);
// 	}
// 	return 0;
// }

// static int scullpipe_proc_open(struct inode *inode, struct file *file) {	
// 	// seq_open 函数首先会创建 seq_operations 结构体，随后将结构体里的 start
// 	// next, stop, show 成员分别设置为 single_start, single_next, single_stop
// 	// 以及 scull_read_procmem 函数。最后将file结构体和seq_operations结构体作为
// 	// 参数调用 seq_open 函数。

// 	// int seq_open(struct file *, const struct seq_operations *);
// 	// seq_open 函数首先会创建一个 seq_file 结构体，同时将传入的 seq_operations 结构体
// 	// 的地址赋值给 seq_file 结构体的op成员，随后将 seq_file 结构体的地址赋值给 file 结构体的
// 	// private_data 成员。这样一来，就建立起了 file->seq_file->seq_operations 三者的关系。
// 	return single_open(file, scull_read_p_mem, NULL);
// }

// proc/scullpipe 文件操作函数
// static struct file_operations scullpipe_proc_ops = {
// 	.owner   = THIS_MODULE,
// 	.open    = scullpipe_proc_open,
// 	.read    = seq_read,
// 	.llseek  = seq_lseek,
// 	.release = single_release
// };

// 设备文件操作函数
struct file_operations scull_pipe_fops = {
	.owner =	THIS_MODULE,
	 // 设置为不可定位
	.llseek =	no_llseek,
	.read =		scull_p_read,
	.write =	scull_p_write,
	.poll =		scull_p_poll,
	 // .unlocked_ioctl = scull_ioctl,
	.open =		scull_p_open,
	.release =	scull_p_release,
	.fasync =	scull_p_fasync,
};

// 参数index是 scull_pipe 结构体在数组中的索引值
static void scull_p_setup_cdev(struct scull_pipe *dev, int index) {	
	// scull_p_devno 为所分配的第一个设备号
	int err, devno = scull_p_devno + index;
    	// 初始化cdev结构体，将并使得cdev结构体的file_operations指针指向scull_pipe_fops
	cdev_init(&dev->cdev, &scull_pipe_fops);
	// 设置cdev结构体的所有者
	dev->cdev.owner = THIS_MODULE;
	// cdev_add 的作用是向系统增加一个由 devno 代表的字符型设备
	// 若返回成功，则代表该设备能够被内核调用，因此在驱动程序还没完全准备好时不要调用cdev_add
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding scullpipe%d", err, index);
}

/*
 * Initialize the pipe devs; return how many we did.
 */
int scull_p_init(dev_t firstdev)
{
	int i, result;

	// 从"firstdev"开始，连续注册"scull_nr_devs"个由该驱动程序管理的设备号
	// 分配每个设备号时，主设备号保持不变，次设备号则递增
	result = register_chrdev_region(firstdev, scull_p_nr_devs, "scullp");

	// alloc_chrdev_region 函数的返回值小于0则报错
	if (result < 0) {
		printk(KERN_NOTICE "Unable to get scullp region, error %d\n", result);
		return 0;
	}
	// scull_p_devno 为所分配的第一个设备号
	scull_p_devno = firstdev;
	// 获取大小为 scull_nr_devs * sizeof(struct scull_pipe) 个字节的内存块
	// 并返回指向该内存块的指针
	scull_p_devices = kmalloc(scull_p_nr_devs * sizeof(struct scull_pipe), GFP_KERNEL);
	// 若“scull_p_devices”为空代表分配内存失败
	if (scull_p_devices == NULL) {
		// 注销已注册的设备号
		unregister_chrdev_region(firstdev, scull_p_nr_devs);
		return 0;
	}

	// 将 scull_p_devices 为起始地址的 scull_nr_devs * sizeof(struct scull_pipe) 个字节的数据设置为0
	memset(scull_p_devices, 0, scull_p_nr_devs * sizeof(struct scull_pipe));

	// 初始化 scull_pipe 结构体的内容
	for (i = 0; i < scull_p_nr_devs; i++) {
		// 初始化设备的输入等待队列头
		init_waitqueue_head(&(scull_p_devices[i].inq));
		// 初始化设备的输出等待队列头
		init_waitqueue_head(&(scull_p_devices[i].outq));
		// 初始化设备的互斥锁
		mutex_init(&scull_p_devices[i].lock);
		scull_p_setup_cdev(scull_p_devices + i, i);
	}

	// 在proc目录下创建 scullpipie 文件, 并设置对应的处理函数
	// proc_create("scullpipe", 0, NULL, proc_ops_wrapper(&scullpipe_proc_ops, scullpipe_pops));
	return scull_p_nr_devs;
}

/*
 * This is called by cleanup_module or on failure.
 * It is required to never fail, even if nothing was initialized first
 */
void scull_p_cleanup(void) {
	int i;

	// remove_proc_entry("scullpipe", NULL);
	if (!scull_p_devices)
		return; /* nothing else to release */

	for (i = 0; i < scull_p_nr_devs; i++) {
		cdev_del(&scull_p_devices[i].cdev);
		kfree(scull_p_devices[i].buffer);
	}
	kfree(scull_p_devices);
	unregister_chrdev_region(scull_p_devno, scull_p_nr_devs);
	scull_p_devices = NULL; /* pedantic */
}
