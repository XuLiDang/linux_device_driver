#ifndef _SCULL_H_
#define _SCULL_H_

#ifndef SCULL_MAJOR
#define SCULL_MAJOR 0   /* dynamic major by default */
#endif

#ifndef SCULL_NR_DEVS
#define SCULL_NR_DEVS 4    /* scull0 through scull3 */
#endif

#ifndef SCULL_P_NR_DEVS
#define SCULL_P_NR_DEVS 4  /* scullpipe0 through scullpipe3 */
#endif

#ifndef SCULL_QUANTUM
// 表示data指针数组中每一项能够占用多少字节
#define SCULL_QUANTUM 4000
#endif

#ifndef SCULL_QSET
// 表示scull_qset结构体中data指针数组有多少项
#define SCULL_QSET    1000
#endif

// 第6章新增内容，定义缓冲区的大小
 #ifndef SCULL_P_BUFFER
 #define SCULL_P_BUFFER 4000
 #endif

// 用该结构体来存放数据
struct scull_qset {
     // 这里的data会被看作是指针数组的首地址
	void **data;
	struct scull_qset *next;
};

// 通过该结构体来表示设备
struct scull_dev {
	struct scull_qset *data;  /* Pointer to first quantum set */
	int quantum;              // data数组每一项有多少字节的数据
	int qset;                 // 代表对应的每个scull_qset中的data数组有多少项
	unsigned long size;       /* amount of data stored here */
	unsigned int access_key;  /* used by sculluid and scullpriv */
	// 每个设备使用单独的信号量,这样便允许不同设备上的操作可以并行处理
	struct mutex lock;     /* mutual exclusion semaphore     */
	// 字符型设备的结构体
    struct cdev cdev;	  /* Char device structure		*/
};


/*
 * The different configurable parameters
 */
extern int scull_major;     /* main.c */
extern int scull_nr_devs;
extern int scull_quantum;
extern int scull_qset;
// 第6章新增内容, 在pipe.c文件中定义, main.c文件需要用到, 因此要在该头文件中定义
 extern int scull_p_buffer;    /* pipe.c */

/*
 * Prototypes for shared functions
 */
int     scull_trim(struct scull_dev *dev);

ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
                   loff_t *f_pos);
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
                    loff_t *f_pos);
loff_t  scull_llseek(struct file *filp, loff_t off, int whence);

// 第六章(阻塞I/O)新增内容
 int     scull_p_init(dev_t dev);
 void    scull_p_cleanup(void);

#endif /* _SCULL_H_ */