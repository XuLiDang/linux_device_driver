#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/cdev.h>		/*  struct cdev */
#include <linux/uaccess.h>	/* copy_*_user */

#include "scull.h"		/* local definitions */
#include "access_ok_version.h"  // 第6章新增头文件, 该头文件会根据内核版本来选择对应的access_ok方法

int scull_major =   SCULL_MAJOR; // scull_major默认等于0,即代表默认选择"动态分配"方法来获得主设备号
int scull_minor =   0; // 次设备号默认为0
int scull_nr_devs = SCULL_NR_DEVS;	// 设备号个数默认为4
int scull_quantum = SCULL_QUANTUM;
int scull_qset =    SCULL_QSET;

struct scull_dev *scull_devices; /* allocated in scull_init_module */

MODULE_AUTHOR("xulidang");
MODULE_LICENSE("Dual BSD/GPL");

// 释放整个数据区,并且在文件以写入方式打开时由scull_open调用该函数
// 遍历scull_dev结构中的scull_qset链表,并释放对应节点的data域的内容
int scull_trim(struct scull_dev *dev)
{	
	struct scull_qset *next, *dptr;
	int qset = dev->qset;   /* "dev" is not-null */
	int i;
	// 遍历scull_qset结构体链表
	for (dptr = dev->data; dptr; dptr = next) { /* all the list items */
		// 当前scull_qset结构体的数据区域不为空，注意其data域可做看是指向一个指针数组的指针
		if (dptr->data) {
			for (i = 0; i < qset; i++)
				// 释放指针数组中每个指针所指向的内存区域
				kfree(dptr->data[i]);
			// 释放用于存放指针数组的内存区域
			kfree(dptr->data);
			dptr->data = NULL;
		}
		// 指向下一个scull_qset结构体
		next = dptr->next;
		// 释放当前scull_qset结构体
		kfree(dptr);
	}
	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;
	return 0;
}

// 进程打开该驱动程序管理的设备时会调用scull_open函数 
int scull_open(struct inode *inode, struct file *filp)
{
	struct scull_dev *dev; /* device information */

	// 根据scull_dev结构体中的cdev成员的指针(inode->i_cdev)来获取指向scull_dev结构体的指针
	// 即识别正在打开的设备
	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	// 填写file结构体中的private_data域为指向scull_dev结构体的指针
	// file结构体中定义了设备私有的数据结构
	filp->private_data = dev; /* for other methods */

	/* now trim to 0 the length of the device if open was write-only */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY) {
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
		// 清除设备中存储的数据
		scull_trim(dev); /* ignore errors */
		mutex_unlock(&dev->lock);
	}
	return 0;          /* success */
}

int scull_release(struct inode *inode, struct file *filp)
{
	return 0;
}

/*
 * Follow the list
 */
struct scull_qset *scull_follow(struct scull_dev *dev, int n)
{
	struct scull_qset *qs = dev->data;

        /* Allocate first qset explicitly if need be */
	if (! qs) {
		qs = dev->data = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
		if (qs == NULL)
			return NULL;  /* Never mind */
		memset(qs, 0, sizeof(struct scull_qset));
	}

	/* Then follow the list */
	while (n--) {
		if (!qs->next) {
			qs->next = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (qs->next == NULL)
				return NULL;  /* Never mind */
			memset(qs->next, 0, sizeof(struct scull_qset));
		}
		qs = qs->next;
		continue;
	}
	return qs;
}

/*
 * Data management: read and write
 */
// 进程从该驱动程序管理的设备读取数据时会调用该函数
// 该函数一次最多只能读取4000字节的数据，即只会从data数组中指针指向的一块内存区域读取数据
ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos) {

	// 获取scull_dev结构体
	struct scull_dev *dev = filp->private_data; 
	// scull_qset结构体链表节点。
	struct scull_qset *dptr;	
	// data指针数组中每个指针指向的内存区域所占用的字节数 与 scull_qset结构体中data指针数组的长度
	int quantum = dev->quantum, qset = dev->qset;
	// 每个scull_qset结构体最多能表示多少字节的数据
	int itemsize = quantum * qset; 
	int item, s_pos, q_pos, rest;
	ssize_t retval = 0;
	// 锁定当前scull_dev结构体
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;
	// 文件偏移量大于设备的存储数据量
	if (*f_pos >= dev->size)
		goto out;
	// 文件偏移量 + 用户程序要读取的字节数大于该设备的存储数据量
	if (*f_pos + count > dev->size)
		// 修改读取的字节数
		count = dev->size - *f_pos;

	/* find listitem, qset index, and offset in the quantum */
	// item代表对应的scull_qset结构体链表节点编号
	item = (long)*f_pos / itemsize;
	// 获取索引值为item的scull_qset结构体中的数据偏移量
	rest = (long)*f_pos % itemsize;
	// data指针数组的索引值
	s_pos = rest / quantum; 
	// 指针所指向的内存区域的偏移量
	q_pos = rest % quantum;

	/* follow the list up to the right position (defined elsewhere) */
	// 找到对应的scull_qset结构体
	dptr = scull_follow(dev, item);

	if (dptr == NULL || !dptr->data || ! dptr->data[s_pos])
		goto out; /* don't fill holes */

	/* read only up to the end of this quantum */
	// 读取的数据量 大于 data数组中指针指向的内存区域 - 内存区域中的偏移量
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	// 将数据从内核空间缓冲区中拷贝到用户空间
	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count)) {
		retval = -EFAULT;
		goto out;
	}
	// 更新文件偏移量
	*f_pos += count;
	// retval代表已拷贝到用户空间的字节数
	retval = count;
	printk(KERN_ALERT "Debug by xulidang:scull_read()/n");

  out:
	mutex_unlock(&dev->lock);
	return retval;
}

// 进程向该驱动程序管理的设备写入数据时会调用该函数
// 该函数一次最多只能写入4000字节的数据，即只会向data数组中指针指向的一块内存区域写入数据
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{	
	// 获取scull_dev结构体
	struct scull_dev *dev = filp->private_data;
	// scull_qset结构体链表节点。
	struct scull_qset *dptr;
	// data指针数组中每个指针指向的内存区域所占用的字节数 与 scull_qset结构体中data指针数组的长度
	int quantum = dev->quantum, qset = dev->qset;
	// 每个scull_qset结构体最多能表示多少字节的数据
	int itemsize = quantum * qset;
	int item, s_pos, q_pos, rest;
	ssize_t retval = -ENOMEM; /* value used in "goto out" statements */
	// 锁定当前scull_dev结构体
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	/* find listitem, qset index and offset in the quantum */
	// item代表对应的scull_qset结构体链表节点编号
	item = (long)*f_pos / itemsize;
	// 获取索引值为item的scull_qset结构体中的数据偏移量
	rest = (long)*f_pos % itemsize;
	// data指针数组的索引值
	s_pos = rest / quantum; 
	// 指针所指向的内存区域的偏移量
	q_pos = rest % quantum;

	/* follow the list up to the right position */
	// 找到对应的scull_qset结构体
	dptr = scull_follow(dev, item);
	if (dptr == NULL)
		goto out;

	if (!dptr->data) {
		dptr->data = kmalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
		memset(dptr->data, 0, qset * sizeof(char *));
	}

	if (!dptr->data[s_pos]) {
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}

	/* write only up to the end of this quantum */
	// 写入的数据量 大于 data数组中指针指向的内存区域 - 内存区域中的偏移量
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	// 将数据从用户空间中拷贝到内核空间缓冲区
	if (copy_from_user(dptr->data[s_pos]+q_pos, buf, count)) {
		retval = -EFAULT;
		goto out;
	}
	// 更新文件偏移量
	*f_pos += count;
	// retval代表已拷贝到用户空间的字节数
	retval = count;

    	// 若写入数据后，文件偏移量大于当前设备的最大数据量则更新dev结构体保存的数据量大小
	if (dev->size < *f_pos)
		dev->size = *f_pos;

	printk(KERN_ALERT "Debug by xulidang:scull_write()/n");

  out:
	mutex_unlock(&dev->lock);
	return retval;
}

// 修改文件偏移量
loff_t scull_llseek(struct file *filp, loff_t off, int whence)
{
	struct scull_dev *dev = filp->private_data;
	loff_t newpos;

	switch(whence) {
	  case 0: /* SEEK_SET */
		newpos = off;
		break;

	  case 1: /* SEEK_CUR */
		newpos = filp->f_pos + off;
		break;

	  case 2: /* SEEK_END */
		newpos = dev->size + off;
		break;

	  default: /* can't happen */
		return -EINVAL;
	}
	if (newpos < 0) return -EINVAL;
	filp->f_pos = newpos;
	return newpos;
}

// 定义该驱动程序的各种操作函数
struct file_operations scull_fops = {
	.owner =    THIS_MODULE,
	.llseek =   scull_llseek,
	.read =     scull_read,
	.write =    scull_write,
	 // .unlocked_ioctl = scull_ioctl,
	.open =     scull_open,
	.release =  scull_release,
};


// 内核使用cdev结构体在内部表示char设备，因此在操作char设备之前，必须先通过cdev_init函数
// 初始化cdev结构体，并通过cdev_add函数向系统增加一个char设备。 这里的cdev结构体已经嵌入到
// 了该驱动程序定义的scull_dev结构体中
static void scull_setup_cdev(struct scull_dev *dev, int index)
{
	int err, devno = MKDEV(scull_major, scull_minor + index);
   	// 初始化cdev结构体,将并使得cdev结构体的file_operations指针指向scull_fops
	cdev_init(&dev->cdev, &scull_fops);
	// 设置cdev结构体的所有者
	dev->cdev.owner = THIS_MODULE;
	// cdev结构体的file_operations指针指向scull_fops(可能重复了)
	dev->cdev.ops = &scull_fops;
	// cdev_add的作用是向系统增加一个由"devno"代表的字符型设备
	// 若返回成功,则代表该设备能够被内核调用,因此在驱动程序还没完全准备好时,不要调用cdev_add
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding scull%d", err, index);
}

// 卸载模块时调用该函数
void scull_cleanup_module(void)
{
	int i;
	// MKDEV 宏会将主设备号与次设备号合并成 dev_t 类型的变量
	dev_t devno = MKDEV(scull_major, scull_minor);

	/* Get rid of our char dev entries */
	if (scull_devices) {
		for (i = 0; i < scull_nr_devs; i++) {
			scull_trim(scull_devices + i);
			// 从系统中移除一个char设备
			cdev_del(&scull_devices[i].cdev);
		}
		// 释放scull_devices指针指向的内存块
		kfree(scull_devices);
	}

	/* cleanup_module is never called if registering failed */
	// 释放所分配的设备号
	unregister_chrdev_region(devno, scull_nr_devs);

	scull_p_cleanup();

}
int scull_init_module(void)
{
	int result, i;
	// dev_t 类型的变量用于保存设备号，包括主设备号与次设备号，从2.6.0版本开始，dev_t 一共占据32个比特位
	// 其中12个位用于保存主设备号，20位用于保存次设备号。通常情况下使用 MAJOR 宏从 dev_t 类型的变量中获取
	// 主设备号，并且使用 MINOR 宏从 dev_t 类型的变量中获取次设备号。
	dev_t dev = 0;

	/*
	 * Get a range of minor numbers to work with, asking for a dynamic
	 * major unless directed otherwise at load time.
	 */
	// 如果 scull_major = 1 则代表采用静态分配为该驱动程序所管理的设备分配设备号
	if (scull_major) 
	{	
		// MKDEV 宏会将主设备号与次设备号合并成 dev_t 类型的变量
		dev = MKDEV(scull_major, scull_minor);

		// 从"dev"开始，连续注册"scull_nr_devs"个由该驱动程序管理的设备号
		// 分配每个设备号时，主设备号保持不变，次设备号则递增
		result = register_chrdev_region(dev, scull_nr_devs, "scull");
	} 
	// 如果 scull_major != 0 则代表采用动态分配为该驱动程序所管理的设备分配设备号
	else 
	{	
		// 该函数会动态分配一个主设备号，并且与传入的次设备号组合形成设备号
		// dev存放的是动态分配的主设备号与第一个次设备号(0)组合形成的设备号
		// scull_minor 代表分配设备号时初始的次设备号，通常为0。
		result = alloc_chrdev_region(&dev, scull_minor, scull_nr_devs,
				"scull");

		// 从返回的设备号中读取出主设备号，使用 alloc_chrdev_region 函数分配的 
		// scull_nr_devs 个设备号中的主设备号通常相同。
		scull_major = MAJOR(dev);
	}

	// alloc_chrdev_region 函数的返回值小于0则报错
	if (result < 0) 
	{
		printk(KERN_WARNING "scull: can't get major %d\n", scull_major);
		return result;
	}

	// 获取大小为"scull_nr_devs * sizeof(struct scull_dev)"个字节的内存块
	// 并返回执行该内存块的指针，前面注册的每个设备都有一个对应的scull_dev结构体
	scull_devices = kmalloc(scull_nr_devs * sizeof(struct scull_dev), GFP_KERNEL);
	// scull_devices为空代表分配内存失败
	if (!scull_devices) {
		result = -ENOMEM;
		goto fail;  /* Make this more graceful */
	}
	// 将scull_devices为起始地址的"scull_nr_devs * sizeof(struct scull_dev)"个字节的数据设置为0
	memset(scull_devices, 0, scull_nr_devs * sizeof(struct scull_dev));

    // 初始化每个设备对应的scull_dev结构体
	for (i = 0; i < scull_nr_devs; i++) {
		scull_devices[i].quantum = scull_quantum;
		scull_devices[i].qset = scull_qset;
		// 这里要记得设置size的初始值,否则在写入数据更新size的时候会出bug
		scull_devices[i].size = 0;
		// 初始化互斥锁，用于处理多个进程同时访问同一个scull_dev结构体的情况
		mutex_init(&scull_devices[i].lock);
		scull_setup_cdev(&scull_devices[i], i);
	}

	// MKDEV 宏会将主设备号与次设备号合并成 dev_t 类型的变量
	dev = MKDEV(scull_major, scull_minor + scull_nr_devs);
	// scull_p_init为第6章(阻塞I/O)的新增内容
	dev += scull_p_init(dev);
	return 0; /* succeed */

  fail:
	scull_cleanup_module();
	return result;
}

module_init(scull_init_module);
module_exit(scull_cleanup_module);