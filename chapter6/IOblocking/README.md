1. 切换到源文件所在的目录，并执行`make`命令生成`.ko`文件。

2. 执行`insmod`命令，将生成的`.ko`文件插入到内核中。执行完命令后，`/proc/devices`目录下可以查看驱动程序向内核注册的设备名以及设备的主设备号。

3. 执行脚本`scull_load.sh`，从而在`/dev`目录下生成多个设备文件节点。

4. 测试Blocking I/O：编译userspace目录下的`test_scullp_read`与`test_scullp_write`源文件，形成对应的可执行文件。随后运行前者的可执行文件，此时可以看到进程会被阻塞。因此需要在新窗口中运行后者的可执行文件，其会向缓冲区写入数据并唤醒被阻塞的进程。

5. poll的相关内容可以参考 https://www.cnblogs.com/zhaobinyouth/p/6252511.html

6. 测试异步通知：编译userspace目录下的`asynctest.c`源文件，形成对应的可执行文件。随后下运行该可执行文件，可以看到其会一直循环等待，直到驱动程序向其发送SIGIO信号。

7. 执行脚本`scull_unload.sh`，移除在`/dev`目录下生成多个设备文件节点。执行`rmnod scull_ch6_io.ko`命令，将模块从内核中卸载