// init.h 则用于指定 initialization 和 cleanup 函数
#include <linux/init.h>
// module.h 包含了可加载模块所需的符号和函数的定义
#include <linux/module.h>
// MODULE_LICENSE 宏用于告诉内核该模块拥有一个免费的许可证，如果没有这样的声明
// 内核就会在加载模块的时候报错
MODULE_LICENSE("Dual BSD/GPL");

// static关键字修饰的函数不会被外部文件所引用
static int hello_init(void)
{
    printk(KERN_ALERT "Hello, world\n");
    return 0;
}

static void hello_exit(void)
{
    printk(KERN_ALERT "Goodbye, cruel world\n");
}

module_init(hello_init);
module_exit(hello_exit);