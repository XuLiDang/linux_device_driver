#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
 
#define log_info(fmt, arg...)	printk("<3>%s:%d " fmt, __FUNCTION__ , __LINE__, ##arg)
 

// 声明slab缓存结构
static struct kmem_cache *slab_cachep;
 
static int __init slab_demo_init(void)
{
    void *object;
 
    log_info("slab cache init \r\n");
        
    // 创建slab高速缓存，并指明每个内存区域的大小均为32字节
    slab_cachep = kmem_cache_create("slab_demo_cache", 
                            32, 0, SLAB_DESTROY_BY_RCU, NULL);
    if (!slab_cachep)
    {
        log_info("error, create slab_cache fail! \r\n");
        return -1;
    }
 
    // 从slab高速缓存中分配内存块(大小均为32字节),并返回指向该内存块首地址的指针
    object = kmem_cache_alloc(slab_cachep, GFP_KERNEL);  
    
    if (object) 
    {
        log_info("create slab object success \r\n");
        // 将所分配的内存区域放回slab高速缓存中
        kmem_cache_free( slab_cachep, object );
    }
    
    return 0;
}
 
static void __exit slab_demo_exit(void)
{   
    // 销毁slab高速缓存, 调用前必须确保所有内存区域都已经返还给slab缓存
    kmem_cache_destroy( slab_cachep );
 
    log_info("slab cache destroy \r\n");
}
 
 
module_init(slab_demo_init);
module_exit(slab_demo_exit);
MODULE_LICENSE("GPL");
