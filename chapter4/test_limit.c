#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void usage()
{
    printf("usage: command \n");
    exit(0);
}

// 向设备写入并读取数据，最大字节数4000
int main(int argc,char **argv)
{
    int i,fd,numbers;
    int *content,*buffer;

    if(argc != 2)
        usage();
        
	// 通过控制台输入需要写入的数据个数
    numbers = atoi(argv[1]);  //将输入转化为int型

    fd = open("/dev/scull0", O_RDWR);

    // 在content指针对应的缓存写数据0~999
    content = (int*)malloc(numbers * sizeof(int)); //写操作以Byte为单位
    for(i = 0; i < numbers; i++)
         *(content + i) = i;

	// 将数据写入scull0
    write(fd, content, numbers * sizeof(int));
    // 指针重新定位到文件开头
    lseek(fd, 0L, SEEK_SET); // lseek 就是file operations结构中的llseek指令

    buffer = (int*)malloc(numbers * sizeof(int));
    // 将scull0中的内容读出到buffer
    read(fd,buffer,numbers * sizeof(int)); 
     
    // 控制台打印
    for(i = 0; i < numbers; i++)
    {
        printf("%d\t", *(buffer + i));
        if((i + 1) % 10 == 0)
        printf("\n");
    }

    printf("\n");
    close(fd);
    return 0;
}