#include <stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<errno.h>
#include<string.h>

void usage()
{
    printf("usage: command \n");
    exit(0);
}

// // 向设备写入并读取数据，最大字节数4000 * 1000
int main(int argc,char **argv)
{
    int i, fd, numbers, count, rest;
    int *content, *buffer, *wcur, *rcur;

    if(argc != 2)
        usage();

    //通过控制台输入需要写入的数据个数
    numbers = atoi(argv[1]); //将输入转化为int型
    rest = numbers*sizeof(int); // rest代表还要写入的数据量

    // 打开设备文件
    fd = open("/dev/scull0",O_RDWR);

    // 申请一块内存缓冲区用于存放要写入文件中的数据
    content = (int*)malloc(numbers * sizeof(int));
    wcur = content;

    if(content < 0)
        printf("malloc failed:%s",strerror(errno));
    // 向缓冲区写入数据
    for(i = 0;i < numbers;i++)
        *(content + i) = i;
    // 向文件写入数据,并用count接收返回值
    count = write(fd, content, numbers * sizeof(int));
 
    //根据write的返回值确认是否需要多次进行写操作。
    while(count != rest)
    {
        if(count < 0) {
            printf("%s\n",strerror(errno));
            exit(0);
        }
        // count < rest 代表还要继续向文件写入数据
        else if(count < rest) {
            rest = rest - count;

            // 除了最后一次之外,每次写入文件操作结束后都需要更新wcur指针
            wcur += count / sizeof(int);
            count = write(fd, wcur, rest);
        }
    }

    //释放malloc申请的内存
    free(content);   
    content = NULL;

    // 指针重新定位到文件开头
    lseek(fd, 0L, SEEK_SET);
    // 这里的rest代表仍需读取的数据量
    rest = numbers * sizeof(int); 

    // 申请一块内存缓冲区用于存放从文件中读取出来的数据
    buffer = (int*)malloc(numbers * sizeof(int));
    rcur = buffer;

    if(buffer < 0)
        printf("malloc failed:%s\n",strerror(errno));
    // 从文件中数据,并用count接收返回值
    count = read(fd, buffer, rest);

    //根据read的返回值确认是否需要多次进行读操作。
    while(count != rest) {
        if(count < 0)
        {
            printf("%s",strerror(errno));
            exit(0);
        }
        // count < rest 代表还要继续从文件读取数据
        else if(count < rest)
        {
            rest -= count;
            // 除了最后一次之外, 每次读取文件操作结束后都需要更新rcur指针
            rcur += count / sizeof(int);
            count = read(fd, rcur, rest);
        }
    }

    // 打印buffer缓冲区的数据
    for(i = 0; i < numbers; i++) {
        printf("%d\t", *(buffer + i));
        if((i + 1) % 10 == 0)
             printf("\n");
    }

    printf("\n");
    // 释放buffer缓冲区
    free(buffer);
    buffer = NULL;

    close(fd);   //关闭scull0
    return 0;
}