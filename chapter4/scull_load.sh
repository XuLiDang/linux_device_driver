#!/bin/sh
device="scull"
mode="777"

# 在不同的机器上，动态分配的主设备号可能会不一样，因此一样要先通过
# "cat /proc/devices" 命令获取scull设备的主设备号
major="511"

# remove stale nodes
rm -f /dev/${device}[0-3]
# create device file through major number we get
mknod /dev/${device}0 c $major 0
mknod /dev/${device}1 c $major 1
mknod /dev/${device}2 c $major 2
mknod /dev/${device}3 c $major 3

# Group: since distributions do it differently, look for wheel or use staff
if grep -q '^staff:' /etc/group; 
then
    group="staff"
else
    group="wheel"
fi

chgrp $group /dev/${device}[0-3]
chmod $mode /dev/${device}[0-3]
