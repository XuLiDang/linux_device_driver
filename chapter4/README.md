1. 切换到源文件所在的目录，并执行`make`命令生成`.ko`文件。

2. 执行`insmod`命令，将生成的`.ko`文件插入到内核中。执行完命令后，`/proc/devices`目录下可以查看驱动程序向内核注册的设备名以及设备的主设备号。

3. 执行脚本`scull_load.sh`，从而在`/dev`目录下生成多个设备文件节点。

4. 编译`test_limit.c`与`test_max.c`源文件，生成对应的可执行文件。前者至多能写入和读取4000字节的数据，而后者至多能写入和读取4000000字节的数据。

5. 向设备文件写入数据后，便可执行`cat /proc/scullmem` 或 `cat /proc/scullseq`命令，从通过proc文件系统读取设备的数据。

6. 执行脚本`scull_unload.sh`，移除在`/dev`目录下生成多个设备文件节点。执行`rmnod scull_ch4.ko`命令，将模块从内核中卸载