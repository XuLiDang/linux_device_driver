#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/cdev.h>		/*  struct cdev */
#include <linux/uaccess.h>	/* copy_*_user */
// 下面两行是第四章新增的
#include <linux/seq_file.h> 
#include <linux/proc_fs.h>

#include "scull.h"		/* local definitions */
#include "proc_ops_version.h" // 第四章新增头文件,用于将file_operation结构体包装成pro_ops结构体

int scull_major =   SCULL_MAJOR; // scull_major默认等于0,即代表默认选择"动态分配"方法来获得主设备号
int scull_minor =   0; // 次设备号默认为0
int scull_nr_devs = SCULL_NR_DEVS;	// 设备号个数默认为4
int scull_quantum = SCULL_QUANTUM;
int scull_qset =    SCULL_QSET;

struct scull_dev *scull_devices; /* allocated in scull_init_module */

MODULE_AUTHOR("xulidang");
MODULE_LICENSE("Dual BSD/GPL");

// 释放整个数据区,并且在文件以写入方式打开时由scull_open调用该函数
// 遍历scull_dev结构中的scull_qset链表,并释放对应节点的data域的内容
int scull_trim(struct scull_dev *dev)
{	
	struct scull_qset *next, *dptr;
	int qset = dev->qset;   /* "dev" is not-null */
	int i;
	// 遍历scull_qset结构体链表
	for (dptr = dev->data; dptr; dptr = next) { /* all the list items */
		// 当前scull_qset结构体的数据区域不为空，注意其data域可做看是指向一个指针数组的指针
		if (dptr->data) {
			for (i = 0; i < qset; i++)
				// 释放指针数组中每个指针所指向的内存区域
				kfree(dptr->data[i]);
			// 释放用于存放指针数组的内存区域
			kfree(dptr->data);
			dptr->data = NULL;
		}
		// 指向下一个scull_qset结构体
		next = dptr->next;
		// 释放当前scull_qset结构体
		kfree(dptr);
	}
	dev->size = 0;
	dev->quantum = scull_quantum;
	dev->qset = scull_qset;
	dev->data = NULL;
	return 0;
}

// 进程打开该驱动程序管理的设备时会调用scull_open函数 
int scull_open(struct inode *inode, struct file *filp)
{
	struct scull_dev *dev; /* device information */

	// 根据scull_dev结构体中的cdev成员的指针(inode->i_cdev)来获取指向scull_dev结构体的指针
	// 即识别正在打开的设备
	dev = container_of(inode->i_cdev, struct scull_dev, cdev);
	// 填写file结构体中的private_data域为指向scull_dev结构体的指针
	// file结构体中定义了设备私有的数据结构
	filp->private_data = dev; /* for other methods */

	/* now trim to 0 the length of the device if open was write-only */
	if ( (filp->f_flags & O_ACCMODE) == O_WRONLY) {
		if (mutex_lock_interruptible(&dev->lock))
			return -ERESTARTSYS;
		// 清除设备中存储的数据
		scull_trim(dev); /* ignore errors */
		mutex_unlock(&dev->lock);
	}
	return 0;          /* success */
}

int scull_release(struct inode *inode, struct file *filp)
{
	return 0;
}

/*
 * Follow the list
 */
struct scull_qset *scull_follow(struct scull_dev *dev, int n)
{
	struct scull_qset *qs = dev->data;

        /* Allocate first qset explicitly if need be */
	if (! qs) {
		qs = dev->data = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
		if (qs == NULL)
			return NULL;  /* Never mind */
		memset(qs, 0, sizeof(struct scull_qset));
	}

	/* Then follow the list */
	while (n--) {
		if (!qs->next) {
			qs->next = kmalloc(sizeof(struct scull_qset), GFP_KERNEL);
			if (qs->next == NULL)
				return NULL;  /* Never mind */
			memset(qs->next, 0, sizeof(struct scull_qset));
		}
		qs = qs->next;
		continue;
	}
	return qs;
}

/*
 * Data management: read and write
 */
// 进程从该驱动程序管理的设备读取数据时会调用该函数
// 该函数一次最多只能读取4000字节的数据，即只会从data数组中指针指向的一块内存区域读取数据
ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
                loff_t *f_pos) {

	// 获取scull_dev结构体
	struct scull_dev *dev = filp->private_data; 
	// scull_qset结构体链表节点。
	struct scull_qset *dptr;	
	// data指针数组中每个指针指向的内存区域所占用的字节数 与 scull_qset结构体中data指针数组的长度
	int quantum = dev->quantum, qset = dev->qset;
	// 每个scull_qset结构体最多能表示多少字节的数据
	int itemsize = quantum * qset; 
	int item, s_pos, q_pos, rest;
	ssize_t retval = 0;
	// 锁定当前scull_dev结构体
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;
	// 文件偏移量大于设备的存储数据量
	if (*f_pos >= dev->size)
		goto out;
	// 文件偏移量 + 用户程序要读取的字节数大于该设备的存储数据量
	if (*f_pos + count > dev->size)
		// 修改读取的字节数
		count = dev->size - *f_pos;

	/* find listitem, qset index, and offset in the quantum */
	// item代表对应的scull_qset结构体链表节点编号
	item = (long)*f_pos / itemsize;
	// 获取索引值为item的scull_qset结构体中的数据偏移量
	rest = (long)*f_pos % itemsize;
	// data指针数组的索引值
	s_pos = rest / quantum; 
	// 指针所指向的内存区域的偏移量
	q_pos = rest % quantum;

	/* follow the list up to the right position (defined elsewhere) */
	// 找到对应的scull_qset结构体
	dptr = scull_follow(dev, item);

	if (dptr == NULL || !dptr->data || ! dptr->data[s_pos])
		goto out; /* don't fill holes */

	/* read only up to the end of this quantum */
	// 读取的数据量 大于 data数组中指针指向的内存区域 - 内存区域中的偏移量
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	// 将数据从内核空间缓冲区中拷贝到用户空间
	if (copy_to_user(buf, dptr->data[s_pos] + q_pos, count)) {
		retval = -EFAULT;
		goto out;
	}
	// 更新文件偏移量
	*f_pos += count;
	// retval代表已拷贝到用户空间的字节数
	retval = count;
	printk(KERN_ALERT "Debug by xulidang:scull_read()/n");

  out:
	mutex_unlock(&dev->lock);
	return retval;
}

// 进程向该驱动程序管理的设备写入数据时会调用该函数
// 该函数一次最多只能写入4000字节的数据，即只会向data数组中指针指向的一块内存区域写入数据
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
                loff_t *f_pos)
{	
	// 获取scull_dev结构体
	struct scull_dev *dev = filp->private_data;
	// scull_qset结构体链表节点。
	struct scull_qset *dptr;
	// data指针数组中每个指针指向的内存区域所占用的字节数 与 scull_qset结构体中data指针数组的长度
	int quantum = dev->quantum, qset = dev->qset;
	// 每个scull_qset结构体最多能表示多少字节的数据
	int itemsize = quantum * qset;
	int item, s_pos, q_pos, rest;
	ssize_t retval = -ENOMEM; /* value used in "goto out" statements */
	// 锁定当前scull_dev结构体
	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;

	/* find listitem, qset index and offset in the quantum */
	// item代表对应的scull_qset结构体链表节点编号
	item = (long)*f_pos / itemsize;
	// 获取索引值为item的scull_qset结构体中的数据偏移量
	rest = (long)*f_pos % itemsize;
	// data指针数组的索引值
	s_pos = rest / quantum; 
	// 指针所指向的内存区域的偏移量
	q_pos = rest % quantum;

	/* follow the list up to the right position */
	// 找到对应的scull_qset结构体
	dptr = scull_follow(dev, item);
	if (dptr == NULL)
		goto out;

	if (!dptr->data) {
		dptr->data = kmalloc(qset * sizeof(char *), GFP_KERNEL);
		if (!dptr->data)
			goto out;
		memset(dptr->data, 0, qset * sizeof(char *));
	}

	if (!dptr->data[s_pos]) {
		dptr->data[s_pos] = kmalloc(quantum, GFP_KERNEL);
		if (!dptr->data[s_pos])
			goto out;
	}

	/* write only up to the end of this quantum */
	// 写入的数据量 大于 data数组中指针指向的内存区域 - 内存区域中的偏移量
	if (count > quantum - q_pos)
		count = quantum - q_pos;

	// 将数据从用户空间中拷贝到内核空间缓冲区
	if (copy_from_user(dptr->data[s_pos]+q_pos, buf, count)) {
		retval = -EFAULT;
		goto out;
	}
	// 更新文件偏移量
	*f_pos += count;
	// retval代表已拷贝到用户空间的字节数
	retval = count;

    // 若写入数据后，文件偏移量大于当前设备的最大数据量则更新dev结构体保存的数据量大小
	if (dev->size < *f_pos)
		dev->size = *f_pos;

	printk(KERN_ALERT "Debug by xulidang:scull_write()/n");

  out:
	mutex_unlock(&dev->lock);
	return retval;
}

// 修改文件偏移量
loff_t scull_llseek(struct file *filp, loff_t off, int whence)
{
	struct scull_dev *dev = filp->private_data;
	loff_t newpos;

	switch(whence) {
	  case 0: /* SEEK_SET */
		newpos = off;
		break;

	  case 1: /* SEEK_CUR */
		newpos = filp->f_pos + off;
		break;

	  case 2: /* SEEK_END */
		newpos = dev->size + off;
		break;

	  default: /* can't happen */
		return -EINVAL;
	}
	if (newpos < 0) return -EINVAL;
	filp->f_pos = newpos;
	return newpos;
}

// 定义该驱动程序的各种操作函数
struct file_operations scull_fops = {
	.owner =    THIS_MODULE,
	.llseek =   scull_llseek,
	.read =     scull_read,
	.write =    scull_write,
	//.unlocked_ioctl = scull_ioctl,
	.open =     scull_open,
	.release =  scull_release,
};


//struct cdev {
//	struct kobject kobj;
//	struct module *owner;
//	const struct file_operations *ops;
//	struct list_head list;
//	dev_t dev;
//	unsigned int count;
//	}
// 内核使用cdev结构体在内部表示char设备，因此在操作char设备之前，必须先通过cdev_init函数
// 初始化cdev结构体，并通过cdev_add函数向系统增加一个char设备。 这里的cdev结构体已经嵌入到
// 了该驱动程序定义的scull_dev结构体中
static void scull_setup_cdev(struct scull_dev *dev, int index) {	
	// MKDEV 宏会将主设备号与次设备号合并成 dev_t 类型的变量
	int err, devno = MKDEV(scull_major, scull_minor + index);
   	// 初始化cdev结构体，并将scull_fops指针的值赋值给cdev结构体的file_operations结构体指针
   	// cdev_init函数定义: https://elixir.bootlin.com/linux/v5.16/C/ident/cdev_init
	cdev_init(&dev->cdev, &scull_fops);
	// 设置cdev结构体的所有者
	dev->cdev.owner = THIS_MODULE;
	// cdev结构体的file_operations指针指向scull_fops(与cdev_init函数执行的操作重复了)
	dev->cdev.ops = &scull_fops;
	// cdev_add的作用是: 向系统增加一个字符型设备，该设备的设备号为devno
	// 若返回成功，则代表该设备能够被内核调用，因此在驱动程序还没完全准备好时不要调用cdev_add
	err = cdev_add (&dev->cdev, devno, 1);
	/* Fail gracefully if need be */
	if (err)
		printk(KERN_NOTICE "Error %d adding scull%d", err, index);
}

// 第四章新增内容！
// #ifdef SCULL_DEBUG 与 #endif 之间的这些代码与prco相关，cat这里主要是使用proc进行程序的调试
// 如果 SCULL_DEBUG 被定义了，那么这些代码就会被编译
#ifdef SCULL_DEBUG

//struct seq_file {
//	char *buf;     //seq_file接口使用的缓存页指针
//	size_t size;   //seq_file接口使用的缓存页大小
//	size_t from;   //从seq_file中向用户态缓冲区拷贝时相对于buf的偏移地址
//	size_t count;  //buf中可以拷贝到用户态的字符数目 
//	loff_t index;  //start、next的处理的下标pos数值
//	loff_t read_pos;  //当前已拷贝到用户态的数据量大小
//	u64 version;
//	struct mutex lock;  //针对此seq_file操作的互斥锁，所有seq_*的访问都会上锁
//	const struct seq_operations *op; //操作实际底层数据的函数
//	void *private;
//	};
// 读取 /proc/scullmem 文件时会执行该函数
int scull_read_procmem(struct seq_file *s, void *v)
{
        int i, j;
        int limit = s->size - 80; /* Don't print more than this */

        // 遍历 scull_dev 结构体数组 scull_devices，并打印每个 scull_dev 结构体的内容
        for (i = 0; i < scull_nr_devs && s->count <= limit; i++) {
        	// 指向设备对应的 scull_dev 结构体
                struct scull_dev *d = &scull_devices[i];
                // 指向 scull_dev 结构体中的第一个 scull_qset 结构体
                struct scull_qset *qs = d->data;

                if (mutex_lock_interruptible(&d->lock))
                        return -ERESTARTSYS;
                // 打印 scull_dev 结构体的信息
                seq_printf(s,"\nDevice %i: qset %i, q %i, sz %li\n",
                             i, d->qset, d->quantum, d->size);
                // 打印所有 scull_qset 结构体的信息
                for (; qs && s->count <= limit; qs = qs->next) 
                { 	/* scan the list */
                        seq_printf(s, "  item at %p, qset at %p\n", qs, qs->data);
                        if (qs->data && !qs->next) /* dump only the last item */
                                for (j = 0; j < d->qset; j++) 
                                {
                                        if (qs->data[j])
                                                seq_printf(s, "    % 4i: %8p\n", j, qs->data[j]);
                                }
                }
                mutex_unlock(&scull_devices[i].lock);
        }
        return 0;
}

/*
 * Here are our sequence iteration methods.  Our "position" is
 * simply the device number.
 */
// struct seq_file {
// char *buf;     //seq_file接口使用的缓存页指针
// size_t size;   //seq_file接口使用的缓存页大小
// size_t from;   //从seq_file中向用户态缓冲区拷贝时相对于buf的偏移地址
// size_t count;  //buf中可以拷贝到用户态的字符数目 
// loff_t index;  //start、next的处理的下标pos数值
// loff_t read_pos;  //当前已拷贝到用户态的数据量大小
// u64 version;
// struct mutex lock;  //针对此seq_file操作的互斥锁，所有seq_*的访问都会上锁
// const struct seq_operations *op; //操作实际底层数据的函数
// void *private;
// };


static void *scull_seq_start(struct seq_file *s, loff_t *pos)
{
	if (*pos >= scull_nr_devs)
		return NULL;   /* No more to read */
	return scull_devices + *pos;
}

static void *scull_seq_next(struct seq_file *s, void *v, loff_t *pos)
{
	(*pos)++;
	if (*pos >= scull_nr_devs)
		return NULL;
	return scull_devices + *pos;
}

static void scull_seq_stop(struct seq_file *s, void *v)
{
	/* Actually, there's nothing to do here */
}

// 读取 /proc/scullseq 文件时会执行该函数
static int scull_seq_show(struct seq_file *s, void *v)
{
	struct scull_dev *dev = (struct scull_dev *) v;
	struct scull_qset *d;
	int i;

	if (mutex_lock_interruptible(&dev->lock))
		return -ERESTARTSYS;
	// 打印 scull_dev 结构体的信息
	seq_printf(s, "\nDevice %i: qset %i, q %i, sz %li\n",
			(int) (dev - scull_devices), dev->qset,
			dev->quantum, dev->size);

	// 展打印所有 scull_qset 结构体的信息
	for (d = dev->data; d; d = d->next) 
	{	 /* scan the list */
		seq_printf(s, "  item at %p, qset at %p\n", d, d->data);

		if (d->data && !d->next) /* dump only the last item */
			for (i = 0; i < dev->qset; i++) 
			{
				if (d->data[i])
					seq_printf(s, "    % 4i: %8p\n", i, d->data[i]);
			}
	}

	mutex_unlock(&dev->lock);
	return 0;
}
	

// seq_read, seq_write, seq_open等函数需要调用里面的函数
static struct seq_operations scull_seq_ops = {
	.start = scull_seq_start,
	.next  = scull_seq_next,
	.stop  = scull_seq_stop,
	.show  = scull_seq_show
};

// 打开 /proc/scullmem 文件时会执行该函数
static int scullmem_proc_open(struct inode *inode, struct file *file)
{	
	// seq_open 函数首先会创建 seq_operations 结构体，随后将结构体里的 start
	// next, stop, show 成员分别设置为 single_start, single_next, single_stop
	// 以及 scull_read_procmem 函数。最后将file结构体和seq_operations结构体作为
	// 参数调用 seq_open 函数。
	return single_open(file, scull_read_procmem, NULL);
}

// 打开 /proc/scullseq 文件时会执行该函数
static int scullseq_proc_open(struct inode *inode, struct file *file)
{	
	// int seq_open(struct file *, const struct seq_operations *);
	// seq_open 函数首先会创建一个 seq_file 结构体，同时将传入的 seq_operations 结构体
	// 的地址赋值给 seq_file 结构体的op成员，随后将 seq_file 结构体的地址赋值给 file 结构体的
	// private_data 成员。这样一来，就建立起了 file->seq_file->seq_operations 三者的关系。
	return seq_open(file, &scull_seq_ops);
}

// 定义 proc 文件的操作函数
static struct file_operations scullmem_proc_ops = {
	.owner   = THIS_MODULE,
	// 进程打开文件时会调用 scullmem_proc_open 函数
	.open    = scullmem_proc_open,
	// 进程读取文件时会调用 seq_read 函数，而该函数需要调用定义在 seq_operations 结构体中的
	// start(), show()以及next()函数，因此在 scullmem_proc_open 函数中需要设置这些函数
	.read    = seq_read,
	// 进程重置文件的偏移量是会调用 seq_lseek 函数
	.llseek  = seq_lseek,
	// 系统释放文件时会调用 seq_release 函数
	.release = single_release
};

// 定义 proc 文件的操作函数
static struct file_operations scullseq_proc_ops = {
	.owner   = THIS_MODULE,
	// 进程打开文件时会调用 scullseq_proc_open 函数
	.open    = scullseq_proc_open,
	// 进程读取文件时会调用 seq_read 函数，而该函数需要调用定义在 seq_operations 结构体中的
	// start(), show()以及next()函数，因此在 scullseq_proc_open 函数中需要设置这些函数
	.read    = seq_read,
	// 进程重置文件的偏移量是会调用 seq_lseek 函数
	.llseek  = seq_lseek,
	// 系统释放文件时会调用 seq_release 函数
	.release = seq_release 
};
	
// 该函数用于创建proc文件
static void scull_create_proc(void)
{	
	// 第二个参数是该文件所在的目录，为NULL则代表该文件会在/proc根目录下创建，第三个参数是实现文件对应的操作函数
	// 而第四个则是需要传递给实现函数的参数。对于第三个参数，5.6之后的内核版本需要传入的是 proc_ops 结构体指针
	// 因此，proc_ops_wrapper 函数用于将 file_operations 结构体成员的值赋值给 proc_ops 结构体对应的成员。
	proc_create_data("scullmem", 0 /* default mode */,
			NULL /* parent dir */, proc_ops_wrapper(&scullmem_proc_ops, scullmem_pops),
			NULL /* client data */);
	proc_create("scullseq", 0, NULL, proc_ops_wrapper(&scullseq_proc_ops, scullseq_pops));
}

// 该函数用于释放proc文件
static void scull_remove_proc(void)
{
	/* no problem if it was not registered */
	remove_proc_entry("scullmem", NULL /* parent dir */);
	remove_proc_entry("scullseq", NULL);
}


#endif /* SCULL_DEBUG */



// 卸载模块时调用该函数
void scull_cleanup_module(void)
{
	int i;
	dev_t devno = MKDEV(scull_major, scull_minor);

	/* Get rid of our char dev entries */
	if (scull_devices) {
		for (i = 0; i < scull_nr_devs; i++) {
			scull_trim(scull_devices + i);
			cdev_del(&scull_devices[i].cdev);
		}
		kfree(scull_devices);
	}


#ifdef SCULL_DEBUG /* use proc only if debugging */
	scull_remove_proc();
#endif

	/* cleanup_module is never called if registering failed */
	unregister_chrdev_region(devno, scull_nr_devs);

}

int scull_init_module(void)
{
	int result, i;
	// dev_t 类型的变量用于保存设备号，包括主设备号与次设备号，从2.6.0版本开始，dev_t 一共占据32个比特位
	// 其中12个位用于保存主设备号，20位用于保存次设备号。通常情况下使用 MAJOR 宏从 dev_t 类型的变量中获取
	// 主设备号，并且使用 MINOR 宏从 dev_t 类型的变量中获取次设备号。
	dev_t dev = 0;

	// 如果 scull_major = 0 则代表采用静态分配为该驱动程序所管理的设备分配设备号
	if (scull_major) 
	{	
		// MKDEV 宏会将主设备号与次设备号合并成 dev_t 类型的变量
		dev = MKDEV(scull_major, scull_minor);

		// 从"dev"开始，连续注册"scull_nr_devs"个由该驱动程序管理的设备号
		// 分配每个设备号时，主设备号保持不变，次设备号则递增
		result = register_chrdev_region(dev, scull_nr_devs, "scull");
	} 
	// 如果 scull_major != 0 则代表采用动态分配为该驱动程序所管理的设备分配设备号
	else 
	{	
		// 该函数会动态分配一个主设备号，并且与传入的次设备号组合形成设备号
		// dev存放的是动态分配的主设备号与第一个次设备号(0)组合形成的设备号
		// scull_minor 代表分配设备号时初始的次设备号，通常为0。
		result = alloc_chrdev_region(&dev, scull_minor, scull_nr_devs,
				"scull");

		// 从返回的设备号中读取出主设备号，使用 alloc_chrdev_region 函数分配的 
		// scull_nr_devs 个设备号中的主设备号通常相同。
		scull_major = MAJOR(dev);
	}

	// alloc_chrdev_region 函数的返回值小于0则报错
	if (result < 0) 
	{
		printk(KERN_WARNING "scull: can't get major %d\n", scull_major);
		return result;
	}

	// 获取大小为"scull_nr_devs * sizeof(struct scull_dev)"个字节的内存块
	// 并返回执行该内存块的指针，前面注册的每个设备都有一个对应的scull_dev结构体
	scull_devices = kmalloc(scull_nr_devs * sizeof(struct scull_dev), GFP_KERNEL);
	// scull_devices为空代表分配内存失败
	if (!scull_devices) {
		result = -ENOMEM;
		goto fail;  /* Make this more graceful */
	}
	// 将scull_devices为起始地址的"scull_nr_devs * sizeof(struct scull_dev)"个字节的数据设置为0
	memset(scull_devices, 0, scull_nr_devs * sizeof(struct scull_dev));

        // 初始化每个设备对应的scull_dev结构体
	for (i = 0; i < scull_nr_devs; i++) {
		scull_devices[i].quantum = scull_quantum;
		scull_devices[i].qset = scull_qset;
		// 这里要记得设置size的初始值,否则在写入数据更新size的时候会出bug
		scull_devices[i].size = 0;
		// 初始化互斥锁，用于处理多个进程同时访问同一个scull_dev结构体的情况
		mutex_init(&scull_devices[i].lock);
		scull_setup_cdev(&scull_devices[i], i);
	}

// 若开启了调试，即定义了 SCULL_DEBUG 的话，便会编译下面的代码
#ifdef SCULL_DEBUG /* only when debugging */
	scull_create_proc();	
#endif
	return 0; /* succeed */

  fail:
	scull_cleanup_module();
	return result;
}

module_init(scull_init_module);
module_exit(scull_cleanup_module);