#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/time.h>
#include <linux/timer.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/types.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/sched/signal.h>
#include <linux/slab.h>
#include <linux/version.h>
#include <asm/hardirq.h>

#include "proc_ops_version.h"

int delay = HZ; /* the default delay, expressed in jiffies */
module_param(delay, int, 0);

MODULE_AUTHOR("xulidang");
MODULE_LICENSE("Dual BSD/GPL");

/* use these as data pointers, to implement four files in one function */
enum jit_files {
	JIT_BUSY,
	JIT_SCHED,
	JIT_QUEUE,
	JIT_SCHEDTO
};


int tdelay = 10;
module_param(tdelay, int, 0);

// 定时器以及tasklet函数所使用的结构体
struct jit_data {
	struct timer_list timer; //定时器结构体
	struct tasklet_struct tlet; // tasklet结构体
	struct seq_file *m;
	int hi; /* tasklet or tasklet_hi */
	wait_queue_head_t wait; // 等待队列头结构体
	unsigned long prevjiffies; // 用于计算定时器执行的间隔
	int loops; // 定时器循环次数
};

#define JIT_ASYNC_LOOPS 5

// 定时器执行函数
void jit_timer_fn(struct timer_list *t)
{	
	// from_timer的作用就类似于container_of宏
	struct jit_data *data = from_timer(data, t, timer);
	// 获得当前的jiffies值
	unsigned long j = jiffies;
	// 这里就相当于执行定时器的主要任务
	seq_printf(data->m, "%9li  %3li     %i    %6i   %i   %s\n",
			     j, j - data->prevjiffies, in_interrupt() ? 1 : 0,
			     current->pid, smp_processor_id(), current->comm);
	// 向内核注册新的定时器,并且执行时间为10个时钟中断后
	if (--data->loops) {
		data->timer.expires += tdelay;
		data->prevjiffies = j;
		add_timer(&data->timer);
	} 
	else 
	{	
		// data->loops = 0 代表不用再注册新的定时器
		// 且当前运行的是最后一个定时器的执行函数,因此需要唤醒阻塞的进程
		wake_up_interruptible(&data->wait);
	}
}

// tasklet执行函数
void jit_tasklet_fn(unsigned long arg)
{	
	// 从参数中获取对应的jit_data结构体
	struct jit_data *data = (struct jit_data *)arg;
	// 获取当前的jiffies值
	unsigned long j = jiffies;
	// 输出信息
	seq_printf(data->m, "%9li  %3li     %i    %6i   %i   %s\n",
			     j, j - data->prevjiffies, in_interrupt() ? 1 : 0,
			     current->pid, smp_processor_id(), current->comm);

	// 重新调度自身
	if (--data->loops) 
	{
		data->prevjiffies = j;
		if (data->hi)
			tasklet_hi_schedule(&data->tlet);
		else
			tasklet_schedule(&data->tlet);
	} 

	// data->loops = 0 代表不用再重新调度
	else 
	{	
		// 唤醒阻塞的进程
		wake_up_interruptible(&data->wait);
	}

}


// /proc/currenttime对应的show方法,用于获取当前执行时间
int jit_currentime_show(struct seq_file *m, void *v)
{
	unsigned long j1;
	u64 j2;

	/* get them four */
	j1 = jiffies;
	j2 = get_jiffies_64();
#if LINUX_VERSION_CODE < KERNEL_VERSION(5, 0, 0)
	{
		struct timeval tv1;
		struct timespec tv2;
		do_gettimeofday(&tv1);
		tv2 = current_kernel_time();
		/* print */
		seq_printf(m, "0x%08lx 0x%016Lx %10i.%06i\n"
		       "%40i.%09i\n",
		       j1, j2,
		       (int) tv1.tv_sec, (int) tv1.tv_usec,
		       (int) tv2.tv_sec, (int) tv2.tv_nsec);
	}
#else
	{
		struct timespec64 tv1;
		struct timespec64 tv2;
		ktime_get_real_ts64(&tv1);
		ktime_get_coarse_real_ts64(&tv2);
		seq_printf(m, "0x%08lx 0x%016Lx %10i.%09i\n"
		       "%40i.%09i\n",
		       j1, j2,
		       (int) tv1.tv_sec, (int) tv1.tv_nsec,
		       (int) tv2.tv_sec, (int) tv2.tv_nsec);

	}
#endif

	return 0;
}

/*
 * This function prints one line of data, after sleeping one second.
 * It can sleep in different ways, according to the data pointer
 */
// /proc/jit*文件对应的show函数,用于进行延迟执行
int jit_fn_show(struct seq_file *m, void *v)
{
	unsigned long j0, j1; /* jiffies */
	wait_queue_head_t wait;
	long data = (long)m->private;

	// 初始化等待队列头
	init_waitqueue_head(&wait);
	j0 = jiffies;
	// 这里delay = HZ,相当于加了一秒
	j1 = j0 + delay;

	switch (data) {
	// 长延迟——忙等待
	case JIT_BUSY:
		// 一直忙等待, 直到jiffies的值等于j1,随后执行cpu_relax()函数
		while (time_before(jiffies, j1))
			cpu_relax();
		break;

	// 长延迟——让出CPU
	case JIT_SCHED:
		// 一直忙等待, 直到jiffies的值等于j1,随后执行schedule()函数
		while (time_before(jiffies, j1))
			schedule();
		break;

	// 长延迟——超时1
	case JIT_QUEUE:
		// 等待指定事件完成,若等待的时间已超过"delay"则直接返回函数
		// 第一个参数是对应的等待队列头,第二个则是等待的事件,这里没有,因此传入的参数为0
		// 第三个参数则是等待的时候,单位是jiffies而不是秒
		wait_event_interruptible_timeout(wait, 0, delay);
		break;

	// 长延迟——超时2
	case JIT_SCHEDTO:
		// 设置当前进程的状态,这样调度器只有在超时到期且进程状态变为TASK_RUNNING时才会运行该进程
		// 如果要实现不可中断的睡眠,可以使用TASK_UNINTERRUPTIBLE
		set_current_state(TASK_INTERRUPTIBLE);
		// wait_event_interruptible_timeout函数在内部会调用schedule_timeout函数
		// 因为不需要等待事件, 因此上面 "长延迟——超时1" 的实现代码可以进行简化
		schedule_timeout(delay);
		break;
	}
	j1 = jiffies; /* actual value after we delayed */

	seq_printf(m, "%9li %9li\n", j0, j1);
	return 0;
}

// /proc/jitimer文件对应的show函数
int jit_timer_show(struct seq_file *m, void *v)
{
	struct jit_data *data;
	// 获取当前jiffies值
	unsigned long j = jiffies;
	// 为jit_data结构体分配内存空间
	data = kmalloc(sizeof(*data), GFP_KERNEL);
	// 分配失败则报错
	if (!data)
		return -ENOMEM;
	// 初始化等待队列头结构体
	init_waitqueue_head(&data->wait);

	/* write the first lines in the buffer */
	// 打印到终端的第一行信息
	seq_puts(m, "   time   delta  inirq    pid   cpu command\n");
	seq_printf(m, "%9li  %3li     %i    %6i   %i   %s\n",
			j, 0L, in_interrupt() ? 1 : 0,
			current->pid, smp_processor_id(), current->comm);

	/* fill the data for our timer function */
	data->prevjiffies = j; 
	data->m = m;
	data->loops = JIT_ASYNC_LOOPS; // 设置定时器循环次数

	// 设置定时器的执行函数,即timer->funtion = jit_timer_fn
	timer_setup(&data->timer, jit_timer_fn, 0);
	// 设置定时器的执行时间,这里设置为再过10个时钟中断就执行定时器
	data->timer.expires = j + tdelay;
	// 向内核添加定时器
	add_timer(&data->timer);

	// 进程睡眠,直到data->loops为0,即等待定时器结束
	wait_event_interruptible(data->wait, !data->loops);
	// 如果该进程是被信号唤醒的,那么返回报错信息
	if (signal_pending(current))
		return -ERESTARTSYS;
	// 释放jit_data结构体占用的内存空间
	kfree(data);
	return 0;
}

// /proc/jitasklet和/proc/jitasklethi对应的show函数
int jit_tasklet_show(struct seq_file *m, void *v)
{
	struct jit_data *data;
	// 获取当前jiffies值
	unsigned long j = jiffies;
	// 从seq_file结构体中取出hi的值	
	long hi = (long)m->private;
	// 为jit_data结构体分配内存空间
	data = kmalloc(sizeof(*data), GFP_KERNEL);
	// 分配失败则报错
	if (!data)
		return -ENOMEM;

	// 初始化等待队列头结构体
	init_waitqueue_head(&data->wait);

	/* write the first lines in the buffer */
	// 打印到终端的第一行信息
	seq_puts(m, "   time   delta  inirq    pid   cpu command\n");
	seq_printf(m, "%9li  %3li     %i    %6i   %i   %s\n",
			j, 0L, in_interrupt() ? 1 : 0,
			current->pid, smp_processor_id(), current->comm);

	/* fill the data for our tasklet function */
	data->prevjiffies = j;
	data->m = m;
	data->loops = JIT_ASYNC_LOOPS; // 设置tasklet函数执行次数

	// 设置tasklet的执行函数,并传递该函数所需的数据
	tasklet_init(&data->tlet, jit_tasklet_fn, (unsigned long)data);

	// 如果访问的是proc/jitasklethi,这里hi的值会为1,hi代表高优先级
	data->hi = hi;
	if (hi)
		// 调度指定的tasklet以高优先级执行
		tasklet_hi_schedule(&data->tlet);
	else
		// 调度执行指定的tasklet
		tasklet_schedule(&data->tlet);

	// 进程睡眠,直到data->loops为0
	wait_event_interruptible(data->wait, !data->loops);
	// 如果该进程是被信号唤醒的,那么返回报错信息
	if (signal_pending(current))
		return -ERESTARTSYS;
	// 释放jit_data结构体占用的内存空间
	kfree(data);
	return 0;
}

// /proc/currenttime对应的open函数
static int jit_currentime_open(struct inode *inode, struct file *file)
{
	// 建立file->seq_file->seq_operation之间的联系
	// seq_operation->show = jit_currentime_show
	// seq_file->ops = seq_operation
	// file->privatedata = seq_file
	return single_open(file, jit_currentime_show, NULL);
}

// /proc/jit*文件对应的open函数
static int jit_fn_open(struct inode *inode, struct file *file)
{
	return single_open(file, jit_fn_show, PDE_DATA(inode));
}

// /proc/jitimer对应的open函数
static int jit_timer_open(struct inode *inode, struct file *file)
{
	return single_open(file, jit_timer_show, NULL);
}

// /proc/jitasklet和/proc/jitasklethi对应的open函数
static int jit_tasklet_open(struct inode *inode, struct file *file)
{
	return single_open(file, jit_tasklet_show, PDE_DATA(inode));
}

// /proc/jit*文件对应的file_operations结构体
static const struct file_operations jit_fn_fops = {
	.open		= jit_fn_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

// /proc/currenttime对应的file_operations结构体
static const struct file_operations jit_currentime_fops = {
	.open		= jit_currentime_open,
	// 读取proc/currentime时,会调用seq_read方法,该方法需要传入file结构体
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

// /proc/jitimer对应的file_operations结构体
static const struct file_operations jit_timer_fops = {
	.open		= jit_timer_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};

// /proc/jitasklet和/proc/jitasklethi对应的file_operations结构体
static const struct file_operations jit_tasklet_fops = {
	.open		= jit_tasklet_open,
	.read		= seq_read,
	.llseek		= seq_lseek,
	.release	= single_release,
};


int __init jit_init(void)
{	
	// 创建/proc/currenttime文件,并定义该文件对应的操作函数,对应于第7章的获取当前时间
	proc_create_data("currentime", 0, NULL,
	    proc_ops_wrapper(&jit_currentime_fops, jit_currentime_pops), NULL);

	// 创建/proc/jitbusy文件,并定义该文件对应的操作函数,对应于第7章的长延迟——忙等待
	proc_create_data("jitbusy", 0, NULL,
	    proc_ops_wrapper(&jit_fn_fops, jit_fn_pops), (void *)JIT_BUSY);

	// 创建/proc/jitsched文件,并定义该文件对应的操作函数,对应于第7章的长延迟——让出CPU
	proc_create_data("jitsched", 0, NULL,
	    proc_ops_wrapper(&jit_fn_fops, jit_fn_pops), (void *)JIT_SCHED);

	// 创建/proc/jitqueue文件,并定义该文件对应的操作函数,对应于第7章的长延迟——超时1
	proc_create_data("jitqueue", 0, NULL,
	    proc_ops_wrapper(&jit_fn_fops, jit_fn_pops), (void *)JIT_QUEUE);

	// 创建/proc/jitschedto文件,并定义该文件对应的操作函数,对应于第7章的长延迟——超时2
	proc_create_data("jitschedto", 0, NULL,
	    proc_ops_wrapper(&jit_fn_fops, jit_fn_pops), (void *)JIT_SCHEDTO);

	// 创建/proc/jitimer文件，并定义该文件对应的操作函数,对应于第7章的定时器
	proc_create_data("jitimer", 0, NULL,
	    proc_ops_wrapper(&jit_timer_fops, jit_timer_pops), NULL);

	// 创建/proc/jitasklet文件，并定义该文件对应的操作函数,对应于第7章的tasklet
	proc_create_data("jitasklet", 0, NULL,
	    proc_ops_wrapper(&jit_tasklet_fops, jit_tasklet_pops), NULL);

	// 创建/proc/jitasklethi文件，并定义该文件对应的操作函数,对应于第7章的tasklet
	proc_create_data("jitasklethi", 0, NULL,
	    proc_ops_wrapper(&jit_tasklet_fops, jit_tasklet_pops), (void *)1);

	return 0; /* success */
}

void __exit jit_cleanup(void)
{	
	remove_proc_entry("currentime", NULL);
	remove_proc_entry("jitbusy", NULL);
	remove_proc_entry("jitsched", NULL);
	remove_proc_entry("jitqueue", NULL);
	remove_proc_entry("jitschedto", NULL);

	remove_proc_entry("jitimer", NULL);
	remove_proc_entry("jitasklet", NULL);
	remove_proc_entry("jitasklethi", NULL);
}

module_init(jit_init);
module_exit(jit_cleanup);
