#ifndef _SCULL_H_
#define _SCULL_H_

#include <linux/ioctl.h> /* needed for the _IOW etc stuff used later */

#ifndef SCULL_MAJOR
#define SCULL_MAJOR 0   /* dynamic major by default */
#endif

#ifndef SCULL_NR_DEVS
#define SCULL_NR_DEVS 4    /* scull0 through scull3 */
#endif

#ifndef SCULL_P_NR_DEVS
#define SCULL_P_NR_DEVS 4  /* scullpipe0 through scullpipe3 */
#endif

#ifndef SCULL_QUANTUM
// data指针数组中每个指针指向的内存区域所占用的字节数
#define SCULL_QUANTUM 4000
#endif

#ifndef SCULL_QSET
// scull_qset结构体中data指针数组的长度
#define SCULL_QSET    1000
#endif

// 用该结构体来存放数据
struct scull_qset {
     // 这里的data会被看作是指针数组的首地址
	void **data;
	struct scull_qset *next;
};

// 通过该结构体来表示设备
struct scull_dev {
	struct scull_qset *data;  /* Pointer to first quantum set */
	int quantum;              /* the current quantum size */
	int qset;                 /* the current array size */
	unsigned long size;       /* amount of data stored here */
	unsigned int access_key;  /* used by sculluid and scullpriv */
	struct mutex lock;     /* mutual exclusion semaphore     */
	// 字符型设备的结构体
    struct cdev cdev;	  /* Char device structure		*/
};


/*
 * The different configurable parameters
 */
extern int scull_major;     /* main.c */
extern int scull_nr_devs;
extern int scull_quantum;
extern int scull_qset;

/*
 * Prototypes for shared functions
 */
int     scull_trim(struct scull_dev *dev);

ssize_t scull_read(struct file *filp, char __user *buf, size_t count,
                   loff_t *f_pos);
ssize_t scull_write(struct file *filp, const char __user *buf, size_t count,
                    loff_t *f_pos);

#endif /* _SCULL_H_ */